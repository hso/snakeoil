import json
import requests
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from diogenes.models import Article, Tag


class Command(BaseCommand):
    help = 'Fetches new stories from HN API'

    def handle(self, *args, **kwargs):
        response = requests.get(settings.HN_API_ENDPOINT_URL)
        if response.status_code == 200:
            data = json.loads(response.content)
            hits = data.get('hits', [])
            for hit in hits:
                tag_ids = []
                for _tag in hit['_tags']:
                    tag, _ = Tag.objects.get_or_create(name=_tag)
                    tag_ids.append(tag.id)
                hit['submitted_at'] = hit.pop('created_at')
                default_fields = ['title',
                                  'url',
                                  'author',
                                  'points',
                                  'story_text',
                                  'num_comments',
                                  'submitted_at']
                article, _ = Article.global_objects.update_or_create(object_id=hit['objectID'],
                                                                     defaults={key: hit[key] for key in hit.keys() & default_fields})
                article.tags.add(*tag_ids)
            self.stdout.write(self.style.SUCCESS(f"Success: {response.status_code}"))
        else:
            raise CommandError(f"Error: {response.status_code}")
