# Snakeoil

This little project is an answer to a Backend Challenge. Implements a
small API that serves recent Python-related articles from "Hacker"
"News".

## Set it up

You'll need docker and docker-compose installed. Refer to your
favorite platform's documentation.

* build image: `DOCKER_BUILDKIT=1 docker build -f Dockerfile . -t snakeoil-api`
* migrate database: `docker-compose run api python3 manage.py migrate`
* create user: `docker-compose run api python3 manage.py createsuperuser` (follow the prompts)
* fetch articles for the first time: `docker-compose run api python3 manage.py fetch`
* run it: `docker-compose up`

## Try it

You can get tokens POSTing your credentials to `/api/token/`:

> $ curl -X POST -H "Content-Type: application/json" -d '{"username": "\<username\>", "password": "\<password\>"}' http://localhost:8080/api/token/

And then use the access token to explore the `/api/`:

> $ curl -H "Authorization: Bearer \<token\>" http://localhost:8080/api/

You can filter the `/api/articles/` endpoint by `author`, `tag` and `title`, passing those as query string params, e.g. `/api/articles/?tag=show_hn`.

You can also filter by month of article submission date to HN using the `month` query param, e.g. `/api/articles/?month=october`.

If you want to remove an article, just DELETE it, e.g. `DELETE /api/articles/1/`. It won't be returned again.

The api service fetches new articles every hour.

## Testing

There is nothing to test.

