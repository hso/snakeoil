from .models import Article, Tag
from rest_framework import serializers


class ArticleSerializer(serializers.HyperlinkedModelSerializer):
    tags = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='name'
    )
    class Meta:
        model = Article
        fields = ['id',
                  'title',
                  'url',
                  'author',
                  'points',
                  'story_text',
                  'num_comments',
                  'submitted_at',
                  'object_id',
                  'tags']


class TagSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tag
        fields = ['id',
                  'name']
