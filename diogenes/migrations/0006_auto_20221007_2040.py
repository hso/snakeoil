# Generated by Django 3.2.16 on 2022-10-07 20:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('diogenes', '0005_alter_article_story_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='deleted_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='article',
            name='is_deleted',
            field=models.BooleanField(default=False),
        ),
    ]
