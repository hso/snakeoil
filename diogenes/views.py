from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.reverse import reverse
from .models import Article, Tag
from .serializers import ArticleSerializer, TagSerializer
from .filters import ArticleFilter


class ArticleViewSet(viewsets.ModelViewSet):
    """
    Article resource
    """
    queryset = Article.objects.prefetch_related('tags').order_by('-created_at')
    serializer_class = ArticleSerializer
    permission_classes = [IsAuthenticated]
    filterset_class = ArticleFilter


class TagViewSet(viewsets.ModelViewSet):
    """
    Tag resource
    """
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    permission_classes = [IsAuthenticated]
