from django_filters import rest_framework as filters
from .models import Article


class ArticleFilter(filters.FilterSet):
    title = filters.CharFilter(
        label='title', field_name='title', lookup_expr='icontains'
    )
    author = filters.CharFilter(
        label='author', field_name='author', lookup_expr='icontains'
    )
    tag = filters.CharFilter(
        label='tag', field_name='tags__name', lookup_expr='exact'
    )
    month = filters.NumberFilter(
        label='month', field_name='submitted_at', lookup_expr='month'
    )

    class Meta:
        model = Article
        fields = (
            'title',
            'author',
            'tag',
            'submitted_at'
        )
