from django.db import models
from django_softdelete.models import SoftDeleteModel


class Article(SoftDeleteModel):
    title = models.CharField(max_length=255)
    url = models.URLField(max_length=255, null=True)
    author = models.CharField(max_length=255)
    points = models.PositiveIntegerField()
    story_text = models.TextField(null=True)
    num_comments = models.PositiveIntegerField()
    submitted_at = models.DateTimeField()
    object_id = models.CharField(max_length=255)
    tags = models.ManyToManyField('Tag')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        indexes = [models.Index(fields=['object_id'])]


class Tag(models.Model):
    name = models.CharField(max_length=255)
