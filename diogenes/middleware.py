def los_meses(get_response):
    MONTHS = {'january': 1,
              'february': 2,
              'march': 3,
              'april': 4,
              'may': 5,
              'june': 6,
              'july': 7,
              'august': 8,
              'september': 9,
              'october': 10,
              'november': 11,
              'december': 12}
    
    def middleware(request):
        month = request.GET.get('month', None)
        if month:
            try:
                int(month)
            except ValueError:
                request.GET = request.GET.copy()
                month_number = MONTHS.get(month.lower(), None)
                if month_number:
                    request.GET['month'] = month_number
                else:
                    del request.GET['month']
        response = get_response(request)
        return response
    return middleware
