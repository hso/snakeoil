from django.apps import AppConfig


class DiogenesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'diogenes'
