FROM python:3.10

ENV PYTHONBUFFERED=1
ENV PROJECT_DIR=/app/

RUN mkdir ${PROJECT_DIR}
COPY . ${PROJECT_DIR}
COPY --chmod=755 entrypoint.sh /entrypoint.sh

WORKDIR ${PROJECT_DIR}

EXPOSE 8000 8080

RUN apt install libpq-dev \
 && pip install --no-cache-dir pipenv \
 && pipenv install --system --deploy \
 && pipenv --clear \
 && find /usr/local \
    \( -type d -a -name test -o -name tests \) \
    -o \( -type f -a -name '*.pyc' -o -name '*.pyo' \) \
    -exec rm -rf '{}' + \
 && useradd django \
 && chown -R django ${PROJECT_DIR}

USER django

ENTRYPOINT ["/entrypoint.sh"]

CMD ["uwsgi", "--http", "0.0.0.0:8080", "--master", "--enable-threads", "--module", "snakeoil.wsgi:application"]
