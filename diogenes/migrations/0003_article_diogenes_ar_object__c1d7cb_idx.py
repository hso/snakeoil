# Generated by Django 3.2.16 on 2022-10-07 14:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('diogenes', '0002_article_object_id'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='article',
            index=models.Index(fields=['object_id'], name='diogenes_ar_object__c1d7cb_idx'),
        ),
    ]
